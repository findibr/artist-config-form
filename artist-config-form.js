(function() {
	'use strict'

	angular.module('findi.artistConfigForm', ['ionic', 'formlyIonic',
			'ui.utils.masks',
			'fixMaskInput'
		])
		.component('artistConfigForm', {
			template: '<form name="{{$ctrl.opts.formName}}" novalidate="novalidate" ng-submit="$ctrl.opts.submit($ctrl.ngModel)">' +
				'<formly-form model="$ctrl.ngModel" fields="$ctrl.opts.fields"></formly-form>' +
				'<input class="findi-btn" type="submit" value="{{$ctrl.opts.submitButtonText}}"/>' +
				'<div ng-transclude></div>' +
				'</form>',
			controller: userFormController,
			controllerAs: '$ctrl',
			transclude: true,
			bindings: {
				ngModel: '=',
				options: '='
			}
		})

	function userFormController($scope, formlyConfig) {
		var vm = this
		var optionsParams = angular.copy(vm.options) || {}
		vm.opts = _buildOpts(vm, optionsParams)

		_createInputRangeType(formlyConfig)
	}

	var _createInputRangeType = function(formlyConfig) {
		var formlyTypes = formlyConfig.getTypes()

		if (!formlyTypes['stacked-input-range']) {
			formlyConfig.setType({
				name: 'stacked-input-range',
				template: '<div class="item item-input item-stacked-label">' +
					'<span class="input-label" aria-label="{{options.templateOptions.label}}"' +
					'id="stacked-textarea">{{options.templateOptions.label}}</span>' +
					'<input ng-model="model[options.templateOptions.fromKey]"' +
					'placeholder="{{options.templateOptions.fromPlaceholder}}"' +
					'type="{{options.templateOptions.fromType}}"' +
					'id="stacked-input-range-{{options.templateOptions.fromKey}}"' +
					'name="stacked-input-range-{{options.templateOptions.fromKey}}"' +
					'formly-custom-validation="options.validators" ng-class="options.className" />' +
					'<input ng-model="model[options.templateOptions.toKey]"' +
					'placeholder="{{options.templateOptions.toPlaceholder}}"' +
					'type="{{options.templateOptions.toType}}"' +
					'id="stacked-input-range-{{options.templateOptions.toKey}}"' +
					'name="stacked-input-range-{{options.templateOptions.toKey}}"' +
					'formly-custom-validation="options.validators" ng-class="options.className" />' +
					'</div>'
			})
		}

		if (!formlyTypes['ion-toggle']) {
			formlyConfig.setType({
				name: 'ion-toggle',
				template: '<div class="item item-toggle">{{options.templateOptions.label}}' +
					'<label class="toggle toggle-findi">' +
					'<input type="checkbox" ng-model="model[options.key]"/>' +
					'<div class="track">' +
					'<div class="handle"></div>' +
					'</div>' +
					'</label>' +
					'</div>'
			})
		}

		if (!formlyTypes.photo) {
			formlyConfig.setType({
				name: 'photo',
				template: '<ionic-upload ng-model="model[options.key]"' +
					'options="options.templateOptions.config" formly-custom-validation="options.validators"></ionic-upload>'
			})
		}
	}

	var _buildOpts = function(vm, optionsParams) {
		var opts = {}
		opts.formName = "$ctrl.artistConfigForm"
		opts.configFields = optionsParams.configFields || {}
		opts.submitButtonText = optionsParams.submitButtonText || 'SALVAR'
		opts.fields = _getFields(opts.configFields)
		opts.submit = function(data) {
			optionsParams.submit(data, vm.artistConfigForm)
		}

		return opts
	}

	var _getFields = function(configFields) {
		var array = []
		var configFieldPhoto = configFields.photo || {}
		var activatedConfigField = configFields.activated || {}
		var titleConfigField = configFields.title || {}
		var fromPriceConfigField = configFields.fromPrice || {}
		var toPriceConfigField = configFields.toPrice || {}
		var fromGuaranteeConfigField = configFields.fromGuarantee || {}
		var toGuaranteeConfigField = configFields.toGuarantee || {}
		var descriptionConfigField = configFields.description || {}
		var percentageConfigField = configFields.percentage || {}

		if (!configFieldPhoto.hide) array.push(_getPhotoField(configFieldPhoto))

		if (!activatedConfigField.hide) array.push(_getActivatedField(
			activatedConfigField))

		if (!titleConfigField.hide) array.push(_getTitleField(titleConfigField))

		if (!fromPriceConfigField.hide && !toPriceConfigField.hide)
			array.push(_getPriceField(fromPriceConfigField, toPriceConfigField))

		if (!toGuaranteeConfigField.hide && !fromGuaranteeConfigField.hide)
			array.push(_getGuaranteeField(fromGuaranteeConfigField,
				toGuaranteeConfigField))

		if (!percentageConfigField.hide)
			array.push(_getPercentageField(percentageConfigField))

		if (!descriptionConfigField.hide) array.push(_getDescriptionField(
			descriptionConfigField))

		return array
	}

	var _getPhotoField = function(configField) {
		return {
			key: configField.key ? configField.key : "photo_url",
			type: "photo",
			templateOptions: {
				config: configField ? configField : {}
			}
		}
	}

	var _getActivatedField = function(configField) {
		return {
			key: configField.key ? configField.key : 'activated',
			type: 'ion-toggle',
			templateOptions: {
				label: configField.label ? configField.label : 'Ativo'
			}
		}
	}

	var _getTitleField = function(configField) {
		return {
			key: configField.key ? configField.key : "title",
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'TíTULO'
			}
		}
	}

	var _getPriceField = function(fromPriceConfigField, toPriceConfigField) {
		fromPriceConfigField.validRange = (fromPriceConfigField.validRange ===
			false) ? false : true

		return {
			type: "stacked-input-range",
			className: 'stacked-input-range',
			templateOptions: {
				fromKey: fromPriceConfigField.key ? fromPriceConfigField.key : "from_price",
				toKey: toPriceConfigField.key ? toPriceConfigField.key : "to_price",
				required: fromPriceConfigField.validRange,
				fromType: fromPriceConfigField.fromType ? fromPriceConfigField.fromType : "tel",
				toType: toPriceConfigField.toType ? toPriceConfigField.toType : "tel",
				fromPlaceholder: fromPriceConfigField.placeholder ?
					fromPriceConfigField.placeholder : "R$ 50.000,00",
				toPlaceholder: toPriceConfigField.placeholder ? toPriceConfigField.placeholder : "R$ 60.000,00",
				label: fromPriceConfigField.label ? fromPriceConfigField.label : 'CACHÊ',
				mask: '',
				maskInput: ''
			},
			ngModelAttrs: {
				mask: {
					attribute: 'ui-money-mask'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			},
			validators: {
				range: {
					expression: function($viewValue, $modelValue, scope) {
						function isNull(value) {
							return (value == null || value == undefined)
						}

						if (fromPriceConfigField.validRange) {
							if (scope.fc && scope.fc.length) {
								var fromPrice = scope.fc[0].$$rawModelValue
								var toPrice = scope.fc[1].$$rawModelValue

								if (!isNull(fromPrice) && !isNull(toPrice) && fromPrice <= toPrice) {
									return true
								}
							}
						} else {
							if (scope.fc && scope.fc.length) {
								var fromPrice = scope.fc[0].$$rawModelValue
								var toPrice = scope.fc[1].$$rawModelValue
								if (isNull(fromPrice) && !isNull(toPrice)) return false
								if (!isNull(fromPrice) && isNull(toPrice)) return false
								if (isNull(fromPrice) && isNull(toPrice)) return true

								if (fromPrice <= toPrice) return true
								else return false
							} else {
								return true
							}
						}

						return false
					}
				}
			},
			extras: {
				validateOnModelChange: true
			}
		}
	}

	var _getGuaranteeField = function(fromGuaranteeConfigField,
		toGuaranteeConfigField) {
		fromGuaranteeConfigField.validRange = fromGuaranteeConfigField.validRange ===
			false ? false : true

		return {
			type: "stacked-input-range",
			className: 'stacked-input-range',
			templateOptions: {
				fromKey: fromGuaranteeConfigField.key ? fromGuaranteeConfigField.key : "from_guarantee",
				toKey: toGuaranteeConfigField.key ? toGuaranteeConfigField.key : "to_guarantee",
				required: fromGuaranteeConfigField.validRange,
				fromType: fromGuaranteeConfigField.fromType ? fromGuaranteeConfigField.fromType : "tel",
				toType: toGuaranteeConfigField.toType ? toGuaranteeConfigField.toType : "tel",
				fromPlaceholder: fromGuaranteeConfigField.placeholder ?
					fromGuaranteeConfigField.placeholder : "R$ 20.000,00",
				toPlaceholder: toGuaranteeConfigField.placeholder ?
					toGuaranteeConfigField.placeholder : "R$ 30.000,00",
				label: fromGuaranteeConfigField.label ? fromGuaranteeConfigField.label : 'GARANTIA',
				mask: '',
				maskInput: ''
			},
			ngModelAttrs: {
				mask: {
					attribute: 'ui-money-mask'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			},
			validators: {
				range: {
					expression: function($viewValue, $modelValue, scope) {
						function isNull(value) {
							return (value == null || value == undefined)
						}

						if (fromGuaranteeConfigField.validRange) {
							if (scope.fc && scope.fc.length) {
								var fromGuarantee = scope.fc[0].$$rawModelValue
								var toGuarantee = scope.fc[1].$$rawModelValue

								if (!isNull(fromGuarantee) && !isNull(toGuarantee) && fromGuarantee <=
									toGuarantee) {
									return true
								}
							}
						} else {
							if (scope.fc && scope.fc.length) {
								var fromGuarantee = scope.fc[0].$$rawModelValue
								var toGuarantee = scope.fc[1].$$rawModelValue
								if (isNull(fromGuarantee) && !isNull(toGuarantee)) return false
								if (!isNull(fromGuarantee) && isNull(toGuarantee)) return false
								if (isNull(fromGuarantee) && isNull(toGuarantee)) return true

								if (fromGuarantee <= toGuarantee) return true
								else return false
							} else {
								return true
							}
						}

						return false
					}
				}
			},
			extras: {
				validateOnModelChange: true
			}
		}
	}

	var _getPercentageField = function(configField) {
		return {
			key: configField.key ? configField.key : "percentage",
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "tel",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'PERCENTAGEM BILHETERIA',
				max: 1,
				mask: '2',
				maskInput: ''
			},
			ngModelAttrs: {
				mask: {
					attribute: 'ui-percentage-mask'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			}
		}
	}

	var _getDescriptionField = function(configField) {
		return {
			key: configField.key ? configField.key : "description",
			type: "stacked-textarea",
			templateOptions: {
				required: configField.required ? configField.required : true,
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'DESCRIÇÃO'
			}
		}
	}
})()
